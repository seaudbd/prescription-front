import { Component, OnInit } from '@angular/core';
import { SignInService } from '../services/sign-in.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  model = {
    email: "",
    password: ""
  };

  isInvalidCredentials: boolean = false;
  isServerError: boolean = false;
  message: any;
  serverResponse: any;

  constructor(private signInService: SignInService) { }

  ngOnInit(): void {
  }

  onSubmit() {


    console.log(this.model);

    this.signInService.authenticateSignIn({
      'email': this.model.email,
      'password': this.model.password,
      'device_name': 'Desktop'
    }).subscribe(
      response => {
        console.log(response);
        this.serverResponse = response;
        if (this.serverResponse.success === false) {
          this.isInvalidCredentials = true;
          this.message = 'The provided credentials are incorrect.';
        }
      },
      error => {
        console.log(error);
        if (error.status === 422) {
          this.isInvalidCredentials = true;
          this.message = 'The provided credentials are incorrect.';
        } else {
          this.isServerError = true;
          this.message = 'Server error occurred.';
        }

      }
    );
  }

}
