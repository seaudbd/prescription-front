import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RequestService {

  baseUrl: any = 'http://api.mizanulsarker.com/api/';




  constructor(public http: HttpClient) { }

  public get(requestUri: string) {
    return this.http.get(this.baseUrl + requestUri);
  }

  public post(requestUri: string, data: any) {
    return this.http.post(this.baseUrl + requestUri, data, {
      headers : {
          'Content-Type' : 'application/x-www-form-urlencoded;application/json;charset=UTF-8'
      }
    });
  }


}
