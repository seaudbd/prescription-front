import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class SignInService {

  constructor(public request: RequestService) { }

  authenticateSignIn(data: any) {
    const requestUri = 'authenticate';
    return this.request.post(requestUri, data);
  }
}
